const Truck = require('../models/truck.js');
const truckTypeList = require('../utils/constants/constants.js').truckTypeList;

// desc: add truck for user
// method: POST
// path: '/api/trucks'
const addTruck = async (req, res) => {
  const userId = req.user?._id.toString();
  const truckType = req.body?.type;
  if (req.user?.role !== 'DRIVER') {
    return res
      .status(400)
      .send({message: 'Access denied. Only driver can add truck'});
  } else if (!userId) {
    return res.status(400).send({message: 'userId not provided'});
  } else if (!truckType) {
    return res.status(400).send({message: 'Please enter type of truck'});
  } else if (!truckTypeList.includes(truckType)) {
    return res.status(400).send({message: 'Improper truck type'});
  }

  try {
    const truckOnLoad = await Truck.find({assigned_to: userId, status: 'OL'});
    if (truckOnLoad.length > 0) {
      return res
        .status(400)
        .send({message: `Driver can't interact with trucks while on a load`});
    }
    await Truck.create({
      created_by: userId,
      type: truckType,
    });
    res.send({message: 'Truck created successfully'});
  } catch (error) {
    res.status(500).send({message: `Server error: ${error}`});
  }
};

// desc: assign truck to user by id
// method: POST
// path: '/api/trucks/:id/assign'
const assignTruck = async (req, res) => {
  const truckId = req.params?.id;
  const userId = req.user?._id.toString();
  let errorMessage;
  if (req.user?.role !== 'DRIVER') {
    return res
      .status(400)
      .send({message: 'Access denied. Only driver can assign truck'});
  } else if (!truckId) {
    return res.status(400).send({message: 'Please provide truck id'});
  }
  try {
    const assignedTrucks = await Truck.find({assigned_to: userId});
    if (assignedTrucks.length > 0) {
      return res.status(400).send({
        message: `You already have assigned truck with id '${assignedTrucks[0]._id}'`,
      });
    }
    const truckOnLoad = await Truck.find({assigned_to: userId, status: 'OL'});
    if (truckOnLoad.length > 0) {
      return res
        .status(400)
        .send({message: `Driver can't interact with trucks while on a load`});
    }

    const truck = await Truck.findOne({
      created_by: userId,
      _id: truckId,
    })
      .clone()
      .catch(() => {
        errorMessage = 'Truck was not found';
      });
    if (errorMessage) {
      return res.status(400).send({message: errorMessage});
    }

    truck.assigned_to = userId;
    await truck.save();
    res.send({message: 'Truck assigned successfully'});
  } catch (error) {
    res.status(500).send({message: `Server error: ${error}`});
  }
};

// desc: get user's trucks
// method: GET
// path: '/api/trucks'
const getTrucks = async (req, res) => {
  if (req.user?.role !== 'DRIVER') {
    return res
      .status(400)
      .send({message: 'Access denied. Only driver can get truck list'});
  }
  try {
    const trucks = await Truck.find({
      created_by: req.user?._id.toString(),
    }).select('-__v');
    res.send({trucks});
  } catch (error) {
    res.status(500).send({message: `Server error: ${error}`});
  }
};

// desc: get user's truck by id
// method: GET
// path: '/api/trucks/:id'
const getTruckById = async (req, res) => {
  const id = req.params?.id;
  const userId = req.user?._id.toString();
  let errorMessage;
  if (req.user?.role !== 'DRIVER') {
    return res
      .status(400)
      .send({message: 'Access denied. Only driver can search truck'});
  } else if (!id) {
    return res.status(400).send({message: 'Please, provide truck id'});
  }
  let truck;
  truck = await Truck.findOne({_id: id, created_by: userId})
    .select('-__v')
    .clone()
    .catch(() => {
      errorMessage = 'Truck was not found';
    });
  if (errorMessage) {
    return res.status(400).send({message: errorMessage});
  }
  res.send({truck});
};

// desc: update user's truck by id
// method: PUT
// path: '/api/trucks/:id'
const updateTruck = async (req, res) => {
  const id = req.params?.id;
  const userId = req.user?._id.toString();
  const truckType = req.body?.type;
  let errorMessage;
  if (req.user?.role !== 'DRIVER') {
    return res
      .status(400)
      .send({message: 'Access denied. Only driver can edit truck info'});
  } else if (!id) {
    return res.status(400).send({message: 'Please, provide truck id'});
  } else if (!truckType) {
    return res.status(400).send({message: 'Please enter type of truck'});
  } else if (!truckTypeList.includes(truckType)) {
    return res.status(400).send({message: 'Improper truck type'});
  }
  try {
    const truckOnLoad = await Truck.find({assigned_to: userId, status: 'OL'});
    if (truckOnLoad.length > 0) {
      return res
        .status(400)
        .send({message: `Driver can't interact with trucks while on a load`});
    }
    await Truck.findOneAndUpdate(
      {_id: id, created_by: userId},
      {type: truckType}
    )
      .clone()
      .catch(() => {
        errorMessage = 'Truck was not found';
      });
    if (errorMessage) {
      return res.status(400).send({message: errorMessage});
    }
    res.send({message: 'Truck details changed successfully'});
  } catch (error) {
    res.status(500).send({message: `Server error: ${error}`});
  }
};

// desc: delete user's truck by id
// method: DELETE
// path: '/api/trucks/:id'
const deleteTruck = async (req, res) => {
  const id = req.params?.id;
  const userId = req.user?._id.toString();
  let errorMessage;
  if (req.user?.role !== 'DRIVER') {
    return res
      .status(400)
      .send({message: 'Access denied. Only driver can delete truck'});
  } else if (!id) {
    return res.status(400).send({message: 'Please, provide truck id'});
  }
  try {
    const truckOnLoad = await Truck.find({assigned_to: userId, status: 'OL'});
    if (truckOnLoad.length > 0) {
      return res
        .status(400)
        .send({message: `Driver can't interact with trucks while on a load`});
    }
    await Truck.findOneAndDelete({_id: id, created_by: userId})
      .clone()
      .catch(() => {
        errorMessage = 'Truck was not found';
      });
    if (errorMessage) {
      return res.status(400).send({message: errorMessage});
    }
    res.send({message: 'Truck deleted successfully'});
  } catch (error) {
    res.status(500).send({message: `Server error: ${error}`});
  }
};

module.exports = {
  addTruck,
  assignTruck,
  getTrucks,
  getTruckById,
  updateTruck,
  deleteTruck,
};
