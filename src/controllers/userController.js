const bcrypt = require('bcrypt');
const User = require('../models/user.js');
const Truck = require('../models/truck.js');

// desc: get user's profile information
// method: GET
// path: '/api/users/me'
const getProfileInfo = async (req, res) => {
  const id = req.user?._id.toString();
  if (!id) {
    return res.status(400).send({message: 'Id of user not provided'});
  }
  try {
    const user = await User.findById(id).select('-password -__v');
    res.send({user});
  } catch (error) {
    res.status(500).send({message: `Server error: ${error}`});
  }
};

// desc: change user's password
// method: PATCH
// path: '/api/users/me/password'
const changePassword = async (req, res) => {
  const id = req.user?._id.toString();
  const {oldPassword, newPassword} = req.body;
  if (!id) {
    return res.status(400).send({message: `User doesn't exist`});
  } else if (!oldPassword || !newPassword) {
    return res.status(400).send({message: `Password not provided`});
  }
  try {
    const user = await User.findById(id);
    // console.log('user', user);
    const isOldPasswordRight = await bcrypt.compare(oldPassword, user.password);
    if (isOldPasswordRight) {
      user.password = await bcrypt.hash(newPassword, 10);
      await user.save();
      res.send({message: 'Password changed successfully'});
    } else {
      res.status(400).send({message: 'Wrong password. Try again'});
    }
  } catch (error) {
    res.status(500).send({message: `Server error: ${error}`});
  }
};

// desc: delete user's profile
// method: DELETE
// path: '/api/users/me'
const deleteProfile = async (req, res) => {
  const id = req.user?._id.toString();
  if (!id) {
    return res.status(400).send({message: `User doesn't exist`});
  }
  try {
    await User.findByIdAndDelete(id);
    await Truck.deleteMany({created_by: id});
    res.send({message: 'Profile deleted successfully'});
  } catch (error) {
    res.status(500).send({message: `Server error: ${error}`});
  }
};

module.exports = {getProfileInfo, changePassword, deleteProfile};
