const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/user.js');
const userValidator = require('../utils/scripts/validators.js').userValidator;

const generateToken = (id) => {
  return jwt.sign({id}, process.env.JWT_SECRET, {
    expiresIn: '240h',
  });
};
// desc: register new user (Shipper or Driver)
// method: POST
// path: '/api/auth/register'
const registerUser = async (req, res) => {
  const email = req.body?.email;
  const password = req.body?.password;
  const role = req.body?.role;
  // return object if validation not passed
  const validationError = userValidator(email, password, role);
  if (validationError) {
    return res.status(400).send({message: validationError.message});
  }
  // to check, what happened, if role set to improper
  // add validators to email, password and role
  try {
    const hashedPassword = await bcrypt.hash(password, 10);
    await User.create({
      email,
      password: hashedPassword,
      role,
    });
    res.send({message: 'Profile created successfully'});
  } catch (error) {
    return res
      .status(500)
      .send({message: `User not created. Server error: ${error}`});
  }
};

// desc: login
// method: POST
// path: '/api/auth/login'
const login = async (req, res) => {
  const email = req.body?.email;
  const password = req.body?.password;
  const validationError = userValidator(email, password, null);
  if (validationError) {
    return res.status(400).send({message: validationError.message});
  }
  try {
    // getting user's password and _id
    const user = await User.findOne({email}).select('password _id');
    const hashedPassword = user?.password;
    const userId = user?._id;
    if (!user || !hashedPassword) {
      return res.status(400).send({message: 'Invalid credentials'});
    }
    const passwordComparingResult = await bcrypt.compare(
      password,
      hashedPassword
    );
    if (!passwordComparingResult) {
      return res.status(400).send({message: 'Invalid credentials'});
    }
    const token = generateToken(userId);
    res.send({jwt_token: token});
  } catch (error) {
    res.status(500).send({message: `Server error: ${error}`});
  }
};

// desc: reset forgotten password
// method: POST
// path: '/api/auth/forgot_password'
const resetPassword = async (req, res) => {
  const email = req.body?.email;
  if (!email) {
    return res.status(400).send({message: 'Please enter a email'});
  }
  try {
    const user = await User.findOne({email}).select('-password');
    if (!user) {
      return res.status(400).send({message: 'Email not found'});
    }
    // placeholder for future functionality to send new password
    res.send({message: 'New password sent to your email address'});
  } catch (error) {
    res.status(500).send({message: `Server error: ${error}`});
  }
};

module.exports = {registerUser, login, resetPassword};
