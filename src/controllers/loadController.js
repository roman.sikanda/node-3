const load = require('../models/load.js');
const Load = require('../models/load.js');
const Truck = require('../models/truck.js');
const loadValidator = require('../utils/scripts/validators.js').loadValidator;
const findAppropriateTruckType =
  require('../utils/scripts/validators.js').findAppropriateTruckType;
const createLoadLog = require('../utils/logs/logger.js').createLoadLog;

const truckList = {
  SPRINTER: {
    length: 300,
    width: 170,
    height: 250,
    payload: 1700,
  },
  ['SMALL STRAIGHT']: {
    length: 500,
    width: 170,
    height: 250,
    payload: 2500,
  },
  ['LARGE STRAIGHT']: {
    length: 700,
    width: 200,
    height: 350,
    payload: 4000,
  },
};
const loadStatusList = ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'];
const loadStateList = [
  'En route to Pick Up',
  'Arrived to Pick Up',
  'En route to delivery',
  'Arrived to delivery',
];

// desc: add load for user
// method: POST
// path: '/api/loads'
const addLoad = async (req, res) => {
  const userId = req.user?._id.toString();
  const name = req.body?.name;
  const payload = req.body?.payload;
  const pickup_address = req.body?.pickup_address;
  const delivery_address = req.body?.delivery_address;
  const dimensions = req.body?.dimensions;
  if (req.user?.role !== 'SHIPPER') {
    return res
      .status(400)
      .send({message: 'Access denied. Only shipper can add load'});
  } else if (!userId) {
    return res.status(400).send({message: 'userId not provided'});
  }
  const validationError = loadValidator(
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions
  );
  if (validationError) {
    return res.status(400).send({message: validationError.message});
  }
  try {
    await Load.create({
      created_by: userId,
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions,
    });
    res.send({message: 'Load created successfully'});
  } catch (error) {
    res.status(500).send({message: `Server error: ${error}`});
  }
};

// desc: post user's load by id
// method: POST
// path: '/api/loads/:id/post'
const postLoadById = async (req, res) => {
  const loadId = req.params?.id;
  const shipperId = req.user?._id;
  if (req.user?.role !== 'SHIPPER') {
    return res
      .status(400)
      .send({message: 'Access denied. Only shipper can post load'});
  } else if (!loadId) {
    return res.status(400).send({message: 'Id of load not provided'});
  }
  try {
    const load = await Load.findOneAndUpdate(
      {
        created_by: shipperId,
        _id: loadId,
        status: 'NEW',
      },
      {status: 'POSTED'}
    );
    if (!load) {
      return res.status(400).send({message: 'Found no load to post'});
    }
    createLoadLog('Load posted successfully', loadId);
    // now stage to search driver with truck from list
    const appropriateTruckTypeList = findAppropriateTruckType(load, truckList);
    const trucks = await Truck.find({
      status: 'IS',
      assigned_to: {$type: 'string'},
      type: {$in: appropriateTruckTypeList},
    });
    if (!trucks || trucks.length === 0) {
      load.status = 'NEW';
      await load.save();
      createLoadLog('No free drivers found after posting', loadId);
      return res.send({
        message: 'Load posted successfully',
        driver_found: false,
      });
    }
    const searchedTruck = trucks[0];
    const driverId = searchedTruck.created_by;
    load.status = 'ASSIGNED';
    load.assigned_to = driverId;
    searchedTruck.status = 'OL';
    load.state = loadStateList[0];
    await load.save();
    await searchedTruck.save();
    createLoadLog(`Log assigned to driver with id ${driverId}`, loadId);
    res.send({
      message: 'Load posted successfully',
      driver_found: true,
    });
  } catch (error) {
    res.status(500).send({message: `Server error: ${error}`});
  }
};

// desc: get user's active load (if exists)
// method: GET
// path: '/api/loads/active'
const getActiveLoad = async (req, res) => {
  const driverId = req.user?._id.toString();
  if (req.user?.role !== 'DRIVER') {
    return res
      .status(400)
      .send({message: 'Access denied. Only driver can get active load'});
  }
  try {
    const activeLoad = await Load.findOne({
      assigned_to: driverId,
      status: 'ASSIGNED',
    }).select('-__v');
    if (!activeLoad) {
      return res.status(400).send({message: 'You have no active loads'});
    }
    res.send({load: activeLoad});
  } catch (error) {
    res.status(500).send({message: `Server error: ${error}`});
  }
};

// desc: get user's load by id
// method: GET
// path: '/api/loads/:id'
const getLoadById = async (req, res) => {
  const loadId = req.params?.id;
  const userId = req.user?._id.toString();
  let errorMessage;
  if (!loadId) {
    return res.status(400).send({message: 'Load id was not found'});
  } else if (!userId) {
    return res.status(400).send({message: 'You are not authorized'});
  }
  try {
    if (req.user?.role === 'DRIVER') {
      const load = await Load.findOne({assigned_to: userId, _id: loadId})
        .clone()
        .catch((error) => (errorMessage = 'Load not found'));
      if (errorMessage) {
        return res.status(400).send({message: errorMessage});
      }
      return res.send({load});
    }
    if (req.user?.role === 'SHIPPER') {
      let load = await Load.findOne({created_by: userId, _id: loadId})
        .clone()
        .catch((error) => (errorMessage = 'Load not found'));
      if (errorMessage) {
        return res.status(400).send({message: errorMessage});
      }
      res.send({load});
    }
  } catch (error) {
    res.status(500).send({message: `Server error: ${error}`});
  }
};

// desc: get user's loads
// method: GET
// path: '/api/loads'
const getLoads = async (req, res) => {
  const userId = req.user?._id.toString();
  const status = req.query?.status;
  const limit = +req.query?.limit || 10;
  const offset = +req.query?.offset || 0;
  if (!userId) {
    return res.status(400).send({message: 'You are not authorized'});
  }
  if (status && !loadStatusList.includes(status)) {
    return res.status(400).send({message: 'Wrong value of parameter "status"'});
  }
  if (typeof limit !== 'number' || limit % 1 !== 0) {
    return res.status(400).send({message: 'Wrong type of parameter "limit"'});
  } else if (typeof offset !== 'number' || offset % 1 !== 0) {
    return res.status(400).send({message: 'Wrong type of parameter "offset"'});
  } else if (limit > 50) {
    limit = 50;
  } else if (limit < 0) {
    return res.status.send({
      message: 'Parameter "limit" can not be lower than "1"',
    });
  }
  try {
    if (req.user?.role === 'DRIVER') {
      const loads = await Load.find({assigned_to: userId})
        .skip(offset)
        .limit(limit);
      let filteredLoads = [];
      if (status) {
        filteredLoads = loads.filter((load) => {
          return load.status === status;
        });
      } else {
        filteredLoads = loads;
      }
      return res.send({loads: filteredLoads});
    }
    if (req.user?.role === 'SHIPPER') {
      const loads = await Load.find({created_by: userId})
        .skip(offset)
        .limit(limit);
      let filteredLoads = [];
      if (status) {
        filteredLoads = loads.filter((load) => {
          return load.status === status;
        });
      } else {
        filteredLoads = loads;
      }
      return res.send({loads: filteredLoads});
    }
  } catch (error) {
    res.status(500).send({message: `Server error: ${error}`});
  }
};

// desc: get user's load shipping information by id
// method: GET
// path: '/api/loads/:id/shipping_info'
const getLoadShippingInfo = async (req, res) => {
  const loadId = req.params?.id;
  const userId = req.user?._id.toString();
  if (req.user?.role !== 'SHIPPER') {
    return res.status(400).send({
      message: 'Access denied. Only shipper can get load shipping info',
    });
  }
  if (!loadId) {
    return res.status(400).send({message: 'Id of load not provided'});
  } else if (!userId) {
    return res.status(400).send({message: 'You are not authorized'});
  }
  try {
    let errorMessage;
    let load = await Load.findOne({created_by: userId, _id: loadId})
      .clone()
      .catch(() => (errorMessage = 'Load not found'));
    if (errorMessage) {
      return res.status(400).send({message: errorMessage});
    }
    return res.send({load});
  } catch (error) {
    res.status(500).send({message: `Server error: ${error}`});
  }
};

// desc: update user's load by id
// method: PUT
// path: '/api/loads/:id'
const updateLoad = async (req, res) => {
  const userId = req.user?._id.toString();
  const loadId = req.params?.id;
  const name = req.body?.name;
  const payload = req.body?.payload;
  const pickup_address = req.body?.pickup_address;
  const delivery_address = req.body?.delivery_address;
  const dimensions = req.body?.dimensions;
  if (req.user?.role !== 'SHIPPER') {
    return res
      .status(400)
      .send({message: 'Access denied. Only shipper can update load'});
  } else if (!userId) {
    return res.status(400).send({message: 'userId not provided'});
  }
  const validationError = loadValidator(
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions
  );
  if (validationError) {
    return res.status(400).send({message: validationError.message});
  }
  try {
    let errorMessage;
    await Load.findOneAndUpdate(
      {created_by: userId, _id: loadId},
      {
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions,
      }
    )
      .clone()
      .catch(() => (errorMessage = 'Load not found'));
    if (errorMessage) {
      return res.status(400).send({message: errorMessage});
    }
    res.send({message: 'Load details changed successfully'});
  } catch (error) {
    res.status(500).send({message: `Server error: ${error}`});
  }
};

// desc: iterate to next load state
// method: PATCH
// path: '/api/loads/active/state'
const iterateLoadState = async (req, res) => {
  const driverId = req.user?._id.toString();
  let loadId;
  let errorMessage;
  if (req.user?.role !== 'DRIVER') {
    return res
      .status(400)
      .send({message: 'Access denied. Only driver can get change load state'});
  }
  try {
    const activeLoad = await Load.findOne({
      assigned_to: driverId,
      status: 'ASSIGNED',
    })
      .select('-__v')
      .clone()
      .catch(() => (errorMessage = 'Load not found'));
    if (errorMessage) {
      return res.status(400).send({message: errorMessage});
    }
    loadId = activeLoad._id;
    const loadStateIndex = loadStateList.findIndex(
      (state) => state === activeLoad.state
    );
    if (loadStateIndex > -1 && loadStateIndex < 2) {
      activeLoad.state = loadStateList[loadStateIndex + 1];
      await activeLoad.save();
    } else if (loadStateIndex === 2) {
      activeLoad.state = loadStateList[loadStateIndex + 1];
      activeLoad.status = loadStatusList[3];
      await activeLoad.save();
      await Truck.findOneAndUpdate(
        {assigned_to: activeLoad.assigned_to},
        {status: 'IS'}
      );
    }
    createLoadLog(`Load state changed to '${activeLoad.state}'`, loadId);
    res.send({message: `Load state changed to ${activeLoad.state}`});
  } catch (error) {
    res.status(500).send({message: `Server error: ${error}`});
  }
};

// desc: delete user's load by id
// method: DELETE
// path: '/api/loads/:id'
const deleteLoad = async (req, res) => {
  const userId = req.user?._id.toString();
  const loadId = req.params?.id;
  let errorMessage;
  if (!loadId) {
    return res.status(400).send({message: 'Id of load not provided'});
  } else if (!userId) {
    return res.status(400).send({message: 'You are not authorized'});
  }
  try {
    await Load.findOneAndDelete({created_by: userId, _id: loadId})
      .clone()
      .catch(() => (errorMessage = 'Load not found'));
    if (errorMessage) {
      return res.status(400).send({message: errorMessage});
    }
    res.send({message: 'Load deleted successfully'});
  } catch (error) {
    res.status(500).send({message: `Server error: ${error}`});
  }
};

module.exports = {
  addLoad,
  postLoadById,
  getLoads,
  getActiveLoad,
  getLoadById,
  getLoadShippingInfo,
  updateLoad,
  iterateLoadState,
  deleteLoad,
};
