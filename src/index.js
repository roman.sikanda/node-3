const express = require('express');
const cors = require('cors');
require('dotenv').config();

const authRouter = require('./routes/auth.js');
const loadsRouter = require('./routes/loads.js');
const trucksRouter = require('./routes/trucks.js');
const usersRouter = require('./routes/users.js');
const loggerMiddleware = require('./utils/logs/logger.js').logger;

const connectDB = require('./utils/db/db.js');
connectDB();

const app = express();
const PORT = process.env.PORT || 8080;

app.use(express.json());
app.use(cors());
app.use(loggerMiddleware);

app.use(authRouter);
app.use(loadsRouter);
app.use(trucksRouter);
app.use(usersRouter);

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
