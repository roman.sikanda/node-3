const userAllowedRoles = ['SHIPPER', 'DRIVER'];
const truckTypeList = ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'];
const loadStatusList = ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'];
const loadStateList = [
  'En route to Pick Up',
  'Arrived to Pick Up',
  'En route to delivery',
  'Arrived to delivery',
];

module.exports = {
  userAllowedRoles,
  truckTypeList,
  loadStatusList,
  loadStateList,
};
