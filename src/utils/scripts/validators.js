const userValidator = (email, password, role) => {
  const allowedRoles = ['SHIPPER', 'DRIVER'];
  const emailRegex = /^\w+@\w/;
  // proper regex: /^\w+@\w+\.\w+$/ , simpler: /^\w+@\w/
  if (!email) {
    return {message: 'Please enter a email'};
  } else if (!password) {
    return {message: 'Please enter a password'};
  } else if (!role && role !== null) {
    return {message: 'Please enter a role'};
  } else if (!allowedRoles.includes(role) && role !== null) {
    return {message: 'Please enter a valid role'};
  } else if (!emailRegex.test(email)) {
    return {message: 'Please enter a valid email'};
  } else {
    return;
  }
};
// may be improved - describing each case thoroughly
// also to check if proper type of input
const loadValidator = (
  name,
  payload,
  pickup_address,
  delivery_address,
  dimensions
) => {
  const {width, length, height} = dimensions;
  if (
    !name ||
    !payload ||
    !pickup_address ||
    !delivery_address ||
    !dimensions
  ) {
    return {message: 'Data for load not provided'};
  } else if (
    !width ||
    width <= 0 ||
    !length ||
    length <= 0 ||
    !height ||
    height <= 0
  ) {
    return {message: 'Provide proper dimensions for load'};
  }
  return;
};
const findAppropriateTruckType = (load, truckList) => {
  const loadWidth = load.dimensions.width;
  const loadHeight = load.dimensions.height;
  const loadLength = load.dimensions.length;
  const payload = load.payload;
  const appropriateTrucks = [];
  for (const truckName in truckList) {
    const truck = truckList[truckName];
    if (
      loadWidth < truck.width &&
      loadHeight < truck.height &&
      loadLength < truck.length &&
      payload < truck.payload
    ) {
      appropriateTrucks.push(truckName);
    }
  }
  return appropriateTrucks;
};
module.exports = {userValidator, loadValidator, findAppropriateTruckType};
