const fs = require('fs');
const Load = require('../../models/load.js');
const filePath = `${__dirname}/logs.txt`;

const writeLog = async (method, path, startTime, statusCode) => {
  try {
    const message = `At ${startTime}, path:'${path}', 
    method: ${method}, status: ${statusCode} \n`;
    if (!fs.existsSync(filePath)) {
      fs.writeFileSync(filePath, '');
    }
    await fs.appendFile(filePath, message, () => {});
  } catch (error) {
    return res
        .status(500)
        .send({message: `Error happened while creating log: ${error}`});
  }
};

const logger = (req, res, next) => {
  res.on('finish', () => {
    writeLog(req.method, req.path, req._startTime, res.statusCode);
  });
  next();
};

const createLoadLog = async (message, loadId)=>{
  const time = new Date().toString();
  console.log(time);
  const load = await Load.findById(loadId);
  if(!load){
    return;
  }
  load.logs.push({message, time});
  await load.save();
}

module.exports = {logger, createLoadLog};