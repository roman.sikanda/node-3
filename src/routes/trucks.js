const express = require('express');
const auth = require('../utils/auth/auth.js');
const {
  addTruck,
  assignTruck,
  getTrucks,
  getTruckById,
  updateTruck,
  deleteTruck,
} = require('../controllers/truckController.js');

const router = new express.Router();

// add truck for user
router.post('/api/trucks', auth, addTruck);

// assign truck to user by id
router.post('/api/trucks/:id/assign', auth, assignTruck);

// get user's trucks
router.get('/api/trucks', auth, getTrucks);

// get user's truck by id
router.get('/api/trucks/:id', auth, getTruckById);

// update user's truck by id
router.put('/api/trucks/:id', auth, updateTruck);

// delete user's truck by id
router.delete('/api/trucks/:id', auth, deleteTruck);

module.exports = router;
