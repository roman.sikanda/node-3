const express = require('express');
const {getProfileInfo, changePassword, deleteProfile} = require('../controllers/userController.js');
const auth = require('../utils/auth/auth.js');
const router = new express.Router();

// get user's profile information
router.get('/api/users/me', auth, getProfileInfo);

// change user's password
router.patch('/api/users/me/password', auth, changePassword);

// delete user's profile
router.delete('/api/users/me', auth, deleteProfile);

module.exports = router;
