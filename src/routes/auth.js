const express = require('express');
const {registerUser, login, resetPassword} = require('../controllers/authController.js');
const router = new express.Router();

// register new system user (Shipper or Driver)
router.post('/api/auth/register', registerUser);

// login
router.post('/api/auth/login', login);

// reset forgotten password
router.post('/api/auth/forgot_password', resetPassword);

module.exports = router;
