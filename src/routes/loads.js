const express = require('express');
const auth = require('../utils/auth/auth.js');
const {
  addLoad,
  postLoadById,
  getLoads,
  getActiveLoad,
  getLoadById,
  getLoadShippingInfo,
  updateLoad,
  iterateLoadState,
  deleteLoad,
} = require('../controllers/loadController.js');

const router = new express.Router();

// add load for user
router.post('/api/loads', auth, addLoad);

// post user's load by id
router.post('/api/loads/:id/post', auth, postLoadById);

// get user's loads
router.get('/api/loads', auth, getLoads);

// get user's active load (if exists)
router.get('/api/loads/active', auth, getActiveLoad);

// get user's load by id
router.get('/api/loads/:id', auth, getLoadById);

// get user's load shipping information by id
router.get('/api/loads/:id/shipping_info', auth, getLoadShippingInfo);

// update user's load by id
router.put('/api/loads/:id', auth, updateLoad);

// iterate to next load state
router.patch('/api/loads/active/state', auth, iterateLoadState);

// delete user's load by id
router.delete('/api/loads/:id', auth, deleteLoad);

module.exports = router;
