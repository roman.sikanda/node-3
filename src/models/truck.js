const mongoose = require('mongoose');
const truckTypeList = require('../utils/constants/constants.js').truckTypeList;
const statusList = ['OL', 'IS'];

const truckSchema = new mongoose.Schema(
  {
    created_by: {
      type: String,
      required: [true, 'Author of record not defined'],
    },
    assigned_to: {
      type: String,
      default: null,
    },
    type: {
      type: String,
      enum: truckTypeList,
      required: [true, 'Type of truck is not defined'],
    },
    status: {
      type: String,
      default: statusList[1],
      enum: statusList,
    },
  },
  {
    timestamps: {
      createdAt: 'created_date',
      updatedAt: false,
    },
  }
);

module.exports = mongoose.model('Truck', truckSchema);
