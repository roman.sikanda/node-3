const mongoose = require('mongoose');
const statusList = require('../utils/constants/constants.js').loadStatusList;
// const statusList = ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'];
const stateList = require('../utils/constants/constants.js').loadStateList;
// const stateList = [
//   'En route to Pick Up',
//   'Arrived to Pick Up',
//   'En route to delivery',
//   'Arrived to delivery',
// ];
const loadSchema = new mongoose.Schema(
  {
    created_by: {
      type: String,
      required: [true, 'Author of record not defined'],
    },
    assigned_to: {
      type: String,
      default: null,
    },
    status: {
      type: String,
      enum: statusList,
      default: statusList[0],
    },
    state: {
      type: String,
      enum: stateList,
    },
    name: {
      type: String,
      required: [true, 'Please, provide name of load'],
    },
    payload: {
      type: Number,
      required: [true, 'Please, provide payload'],
    },
    pickup_address: {
      type: String,
      required: [true, 'Please, provide pickup address'],
    },
    delivery_address: {
      type: String,
      required: [true, 'Please, provide delivery address'],
    },
    dimensions: {
      width: {
        type: Number,
        required: true,
        min: [1, 'Width must be at least 1, got {VALUE}'],
      },
      length: {
        type: Number,
        required: true,
        min: [1, 'Length must be at least 1, got {VALUE}'],
      },
      height: {
        type: Number,
        required: true,
        min: [1, 'Height must be at least 1, got {VALUE}'],
      },
    },
    logs: [{message: String, time: String}],
  },
  {
    timestamps: {
      createdAt: 'created_date',
      updatedAt: false,
    },
  }
);

module.exports = mongoose.model('Load', loadSchema);
