const mongoose = require('mongoose');
const allowedRoles = require('../utils/constants/constants.js').userAllowedRoles;
// const allowedRoles = ['SHIPPER', 'DRIVER'];

const userSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      lowercase: true,
      trim: true,
      required: [true, 'Please enter the username'],
      unique: [true, 'This email was used already'],
    },
    password: {
      type: String,
      required: [true, 'Please enter a password'],
    },
    role: {
      type: String,
      enum: allowedRoles,
      required: [true, 'Please enter a role'],
    },
  },
  {
    timestamps: {
      createdAt: 'created_date',
      updatedAt: false,
    },
  }
);

module.exports = mongoose.model('User', userSchema);
